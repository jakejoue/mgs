# mgs 平台前端组件库

_mgs_ 组件库是基于 _vue_ 基础开发的组件库。包含一些基础组件的易用性封装，基础布局，复杂表单项，可视化表格，地图相关工具等。

**包结构说明**

-   _@mgs/config_ 全局配置，主要是 token 验证，api 请求等工具集合的 url 配置
-   _@mgs/utils_ 工具集合，纯 js 工具包
-   _@mgs/core_ 核心组件库，布局引擎
-   _@mgs/plugins_ 一些公用 vue 的指令，mixin 函数等
-   _@mgs/themes_ 主题配色变量，mgs-icon 库等，所有组件变量定义要从这获取基础变量
-   _@mgs/simulation_ 态势类库，包括一些地图特效渲染行为
-   _@mgs/components_ 业务组件库，包括资源目录树，军标标绘面板等
-   _@mgs/components-basic_ 基础组件包，tree，table，list，form，dialog，panel，viewer
-   _@mgs/components-item_ 自定义表单项，如文件上传，视频生成，tag 列表等
-   _@mgs/components-chart_ 图表相关组件
-   _@mgs/components-layout_ 基础布局
-   _@mgs/components-map_ 地图相关组件
-   _@mgs/components-tool_ 地图工具集合
-   _@mgs/components-mobile_ 移动端相关
