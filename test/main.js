import Vue from 'vue';
Vue.config.productionTip = false;

// token工具方法
import { getToken, checkToken } from '@mgs/utils/request';

// UI 库
import 'element-ui/packages/theme-chalk/src/index.scss';
import ElementUI from 'element-ui';
Vue.use(ElementUI);

// 地图组件库
import 'leaflet/dist/leaflet.css';
import 'mgs-webui-basic-map/styles/index.scss';
import MgsWebuiBasicMap from 'mgs-webui-basic-map';
Vue.use(MgsWebuiBasicMap, {
    service: {
        urlParser(url) {
            const preUrl = url.startsWith('http') ? '' : APPCONFIG.PROXY_URL;
            const userkey = '886e60bb7e014f22a707de23e6f6505d';
            return preUrl + url.replace('{userKey}', userkey);
        },
    },
    log: false,
});

// mgs项目构造器
import Mgs from '@mgs/mgs';
import '@mgs/themes/icon.scss';
Vue.use(Mgs, {
    API_URLS: { api: APPCONFIG.SERVER_IP, dfc: APPCONFIG.DFC_IP },
    CHECK_TOKEN_URL: APPCONFIG.CHECK_TOKEN_URL,
    IMG_URL: APPCONFIG.IMG_URL,
    IMG_AFTER_IMGURL: APPCONFIG.IMG_AFTER_IMGURL,
});

// token验证
import App from './App.vue';
checkToken(getToken())
    .catch(() =>
        fetch(
            APPCONFIG.SERVER_IP +
                '/api/auth/oauth/token?client_id=portal_fh&grant_type=password&username=admin&password=N9dj4j%2Fg5X5zBIWPXcaYOA%3D%3D&client_secret=dgc%2BP55ngF%2F9oreTmSZV3A%3D%3D'
        )
            .then(res => res.json())
            .then(data => localStorage.setItem('token', data.access_token))
            .catch(() => {})
    )
    .then(() => {
        new Vue({
            render: h => h(App),
        }).$mount('#app');
    });
