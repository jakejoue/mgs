/*
 * @Author: 黄威
 * @Date: 2021-11-04 17:47:14
 * @LastEditors: 黄威
 * @LastEditTime: 2021-12-21 11:02:33
 * @Description: dfc 查询相关
 */
import { buildApi } from '@mgs/utils/api';

// dfc结果处理函数
function response(res) {
    return {
        total: res.length,
        list: res.map(item => {
            item = item.address;
            // 构造多边形参数
            item.centroid = { lng: +item.x, lat: +item.y };
            item.geom = {
                type: 'Point',
                coordinates: item.centroid,
            };
            return item;
        }),
    };
}

// dfc查询相关
export default buildApi(
    {
        // 拆词搜索
        splitMatch: {
            url: 'dfc/geocoding/services/fuzz/splitMatch',
            method: 'post',
            data: {
                // 关键字
                address: { type: String, required: true },
                // 分页用
                top: Number,
            },
            params: ['address', 'top'],
            request(config) {
                config.data = [];
                return config;
            },
            response,
        },
        // 通过类型和范围查询标准地址
        addrSearch: {
            url: 'dfc/geocoding/services/addr/search',
            data: {
                shape: Array,
                address: String,
                page: 0,
                size: 100,
            },
            response,
        },
    },
    true
);
