/*
 * @Author: 黄威
 * @Date: 2021-09-17 16:53:15
 * @LastEditors: wanghan
 * @LastEditTime: 2022-01-14 10:19:03
 * @Description: 这里编写后台接口请求方法
 */

import { buildApi } from '@mgs/utils/api';
import { parseStringPromise } from 'xml2js';

async function parseMetaData(metaData) {
    if (!metaData) return null;

    return parseStringPromise(metaData, {
        explicitArray: false,
        mergeAttrs: true,
        normalizeTags: true,
        explicitRoot: false,
    }).then(res => {
        /**
         * @type {Array}
         */
        const list = res.item.item.filter(item => item.show !== 'no');
        list.parent = {};

        const queue = [list];

        while (queue.length) {
            const items = queue.shift();

            for (let i = 0; i < items.length; i++) {
                const { id, value, item } = items[i];

                if (item) {
                    items.parent[id] = item.parent = {};
                    queue.push(item);
                } else {
                    items.parent[id] = value;
                }
            }
        }

        return list.parent;
    });
}

export default buildApi({
    // 获取基础底图配置
    getBaseMap: {
        port: 999002,
        mode: 'queryTreeStruct',
        data: {
            groupId: 101,
        },
        response: res => {
            return res.map(item => {
                return {
                    title: item.label,
                    thumbnail: item.props.serviceurl,
                    services: item.children.map(item2 => {
                        item2.props.servicename = item2.props.basemap_title;
                        item2.props.serviceid = item2.props.pkid;
                        return item2.props;
                    }),
                };
            });
        },
    },
    // 获取工具类
    getTools: {
        port: 999003,
        mode: 'queryList',
        data: {
            groupId: 101,
        },
        response: res => {
            return res.map(item => {
                // 去除键值之间的的下划线
                const newObj = {};
                for (const key in item) {
                    const val = item[key];
                    newObj[key.replaceAll('_', '')] = val;
                }
                // 去除icon后缀
                newObj.toolicon = newObj.toolicon.replace('.png', '');
                return newObj;
            });
        },
    },
    // 获取服务目录树配置
    getServiceTree: {
        port: 999001,
        mode: 'queryList',
        response: function (res) {
            return Promise.all(
                res.map(item =>
                    parseMetaData(item.servicemetadata).then(res => {
                        item.servicemetadata = res;
                        return item;
                    })
                )
            );
        },
    },
    // 获取地图书签列表
    getBookmarkList: {
        url: 'api/system/map/mapBookmark/queryPage',
        method: 'get',
        data: {
            // 关键字
            queryText: String,
            // 分页用
            pageNum: { type: Number, required: true },
            pageSize: { type: Number, required: true },
        },
        params: ['queryText', 'pageNum', 'pageSize'],
        request(config) {
            config.data = [];
            return config;
        },
    },
    // 添加地图书签
    addBookmark: {
        url: 'api/system/map/mapBookmark/add',
        method: 'post',
        data: {
            // 地图名称
            bookmarkTitle: String,
            // 地图参数 Json对象
            bookmarkParams: String,
            // 缩略图 base64
            thumbnail: String,
        },
    },
    // 添加地图书签
    removeBookmark: {
        url: 'api/system/map/mapBookmark/batchDelete',
        method: 'post',
        data: {
            data: Array,
        },
        // params: ['bookmarkTitle', 'bookmarkParams', 'thumbnail'],
        request(config) {
            config.data = config.data.data || [];
            return config;
        },
    },
});
