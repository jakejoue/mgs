/*
 * @Author: 黄威
 * @Date: 2021-11-04 17:47:45
 * @LastEditors: 黄威
 * @LastEditTime: 2021-11-29 17:45:33
 * @Description: es 查询相关
 */
import { buildApi } from '@mgs/utils/api';

export default buildApi({
    // 获取结果展示配置项
    getFieldConfig: {
        port: 999006,
        mode: 'queryList',
        response: res => {
            return res.map(item => {
                // 解析配置
                item.view_fields = JSON.parse(item.view_fileds);
                return item;
            });
        },
    },
    // 关键字 位置查询
    queryPoiByKeyword: {
        port: 999004,
        mode: 'queryList4Page',
        data: {
            queryText: { type: String, required: true },
            pageNo: 0,
            pageSize: 30,
            search_indexes: 'mgs_global_dat_poi',
            search_resultfrom: 'hits',
        },
    },
    // 坐标反差 位置查询
    queryPoiByPos: {
        port: 999005,
        mode: 'queryList4Page',
        data: {
            lon: { type: Number, required: true },
            lat: { type: Number, required: true },
            pageNo: 0,
            pageSize: 30,
            search_indexes: 'mgs_global_dat_poi',
            search_resultfrom: 'hits',
        },
    },
});
