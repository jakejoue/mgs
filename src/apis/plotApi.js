/*
 * @Autor: wanghan
 * @Date: 2022-01-14 14:52:51
 * @LastEditors: wanghan
 * @LastEditTime: 2022-01-21 20:47:10
 * @Description: 标绘api
 */
import { buildApi } from '@mgs/utils/api';

export default buildApi({
    // 获取标绘方案列表
    getPlotPlanList: {
        port: 2013,
        mode: 'queryList4Page',
        data: {
            pageNo: { type: Number, default: 0 },
            pageSize: { type: Number, default: 30 },
        },
    },
    /**
     * 新增标绘方案
     */
    addPlotPlan: {
        port: 2034,
        mode: 'processOne',
        data: {
            pkid: { type: String, required: true },
            topic_name: { type: String, default: '标绘方案' },
            topic_type: { type: Number, default: 0 }, //'专题类型:0：常规专题, 1：汇总专题, 2：共享专题, 3.协同专题, 4.链接专题, 5.挂接专题,6.收藏专题 其中默认创建的是常规专题'
            del_status: { type: Number, default: 0 }, //当前专题状态,0：未删除，1：已删除
        },
    },
    /**
     * 删除标绘方案
     */
    removePlotPlan: {
        port: 2035,
        mode: 'processOne',
        data: {
            pkids: { type: Array, required: true },
        },
    },
    /**
     * 更新标绘方案
     */
    updatePlotPlan: {
        port: 2036,
        mode: 'processOne',
        data: {
            pkids: { type: Array, required: true },
            topic_name: { type: String, default: '标绘方案' },
            topic_type: { type: Number, default: 0 }, //'专题类型:0：常规专题, 1：汇总专题, 2：共享专题, 3.协同专题, 4.链接专题, 5.挂接专题,6.收藏专题 其中默认创建的是常规专题'
            del_status: { type: Number, default: 0 }, //当前专题状态,0：未删除，1：已删除
        },
    },

    /**
     * 获取方案热点数据
     */
    getPlotPlanHotpot: {
        port: 2014,
        mode: 'queryList',
        data: {
            topic_id: { type: String, required: true },
        },
    },
    /**
     * 保存方案热点数据
     */
    savePlotPlanHotpot: {
        port: 2037,
        mode: 'processOne',
        data: {
            pkid: { type: String, required: true },
            topic_id: { type: String, required: true },
            hotpot_name: { type: String, required: true },
            plot_name: { type: String, required: true },
            plot_libid: { type: Number, required: true },
            plot_code: { type: Number, required: true },
            plot_latlngs: { type: String, required: true },
            plot_options: { type: String, required: true },
            plot_customcode: { required: true },
            plot_symboltype: { required: true },
        },
    },
    /**
     * 保存方案热点数据
     */
    updatePlotPlanHotpot: {
        port: 2039,
        mode: 'processOne',
        data: {
            pkid: { type: String, required: true },
            topic_id: { type: String, required: true },
            hotpot_name: { type: String, required: true },
            plot_name: { type: String, required: true },
            plot_libid: { type: Number, required: true },
            plot_code: { type: Number, required: true },
            plot_latlngs: { type: String, required: true },
            plot_options: { type: String, required: true },
            plot_customcode: { required: true },
            plot_symboltype: { required: true },
        },
    },
    /**
     * 删除方案热点数据
     */
    removePlotPlanHotpot: {
        port: 2038,
        mode: 'processOne',
        data: {
            pkids: { type: Array, required: true },
            topic_id: { type: String, required: true }, //专题id
        },
    },

    /**
     * 获取标绘库目录
     */
    getPlotDir: {
        port: 2016,
        mode: 'queryList',
    },
    /**
     * 获取标绘库目录标号
     */
    getPlotDirItem: {
        port: 2017,
        mode: 'queryList',
        data: {
            parent_id: { type: String, required: true },
        },
    },
});
