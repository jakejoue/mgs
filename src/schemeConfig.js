import L from 'leaflet';
import MapAPI from '@/apis/mapApi';

// 构造配置工具类
import { buildConstantProps, buildComputedProps } from '@mgs/utils/helper';

// 二维瓦片默认不重复加载瓦片
L.GridLayer.prototype.options.noWrap = true;
// 地图视图参数（初始化时候坐标）
const view = { center: [108.28125, 34.58496], zoom: 3 };
// 二维地图参数
const mapOptions = {
    minZoom: 2,
    crs: L.CRS.EPSG4326,
    preferCanvas: true,
    dragging: true,
    touchZoom: true,
    zoomControl: false,
    attributionControl: false,
    maxBounds: L.latLngBounds(L.latLng(-90, -360), L.latLng(90, 360)),
};

// bindMap引用
const bindMapProp = buildComputedProps({ bindMap: 'comp-01 bindMap' })[0];

const componentsConfig = [
    // 地图
    {
        id: 'comp-01',
        name: 'MgsMapView',
        type: 'component',
        props: [
            ...buildConstantProps({
                view,
                plugins: ['analysis', 'plotting'],
                pluginOptions: {
                    analysis: APPCONFIG.ANALYSIS_CONFIG,
                    plotting: {
                        url: APPCONFIG.PLOT_URL,
                    },
                },
                options2d: mapOptions,
                options3d: {},
            }),
        ],
    },
    /* ***** 后续业务组件 ***** */
    // 资源目录树
    {
        id: 'comp-02',
        name: 'MgsServiceCatalogView',
        type: 'component',
        props: [bindMapProp],
        events: [
            {
                key: 'service-add',
                receivers: [{ id: 'tool-LayerManager', key: 'add' }],
            },
            {
                key: 'service-remove',
                receivers: [{ id: 'tool-LayerManager', key: 'remove' }],
            },
        ],
        lyConfig: {
            type: 'static',
            name: '资源目录',
            icon: 'mgs-icon-menu',
        },
    },
    // 路径导航
    {
        id: 'comp-03',
        name: 'MgsPathNavView',
        type: 'component',
        props: [bindMapProp],
        events: [
            {
                key: 'track-play',
                receivers: [{ id: 'other-01', key: 'setTrack' }],
            },
        ],
        lyConfig: {
            type: 'static',
            name: '路径导航',
            icon: 'mgs-icon-nav',
        },
    },
    // 军标标绘
    {
        id: 'comp-05',
        name: 'MgsPlottingPlanView',
        type: 'component',
        props: [bindMapProp],
        events: [
            {
                key: 'search',
                receivers: [
                    {
                        id: 'layout-01',
                        key: 'loadModule',
                        sync: true,
                        data: ['comp-05-01'],
                    },
                    { id: 'comp-05-01', key: 'openPanel' },
                ],
            },
        ],
        lyConfig: {
            type: 'static',
            name: '军标管理',
            icon: 'mgs-icon-nav',
        },
    },
    // 地图书签
    {
        id: 'comp-06',
        name: 'MgsBookmarkView',
        type: 'component',
        props: [bindMapProp],
        lyConfig: {
            type: 'static',
            name: '地图书签',
            icon: 'mgs-icon-nav',
        },
    },
    // 检索结果
    {
        id: 'comp-04',
        name: 'MgsGeocodingView',
        type: 'component',
        props: [bindMapProp],
        lyConfig: {
            type: 'dynamic',
            name: '数据查询',
            icon: 'el-icon-s-management',
        },
    },
    /* ***** 后续地图控件 ***** */
    {
        id: 'control-01',
        name: 'MgsBasemapSwitch',
        type: 'component',
        props: [bindMapProp, ...buildConstantProps({ baseMaps: [] })],
        events: [
            {
                key: 'mounted',
                callback(evt) {
                    MapAPI.getBaseMap().then(res => {
                        evt.sender.vm.$parent.baseMaps = res;
                    });
                },
            },
        ],
        lyConfig: {
            type: 'float',
            position: ['bottom', 'right'],
        },
    },
    {
        id: 'control-02',
        name: 'MgsZoom',
        type: 'component',
        props: [bindMapProp],
        lyConfig: {
            type: 'float',
            position: ['bottom', 'right'],
            offset: [60, 13],
        },
    },
    {
        id: 'control-03',
        name: 'MgsMapSwtich',
        type: 'component',
        props: [
            ...buildComputedProps({ mgsMapView: 'comp-01' }),
            ...buildConstantProps({ simple: true }),
        ],
        lyConfig: {
            type: 'float',
            position: ['bottom', 'right'],
            offset: [163, 13],
        },
    },
    {
        id: 'control-04',
        name: 'MgsSearchInput',
        type: 'component',
        props: buildConstantProps({
            placeholder: '请输入地名',
            // formData: { graphic: null, types: [] },
            // formConfigs: [
            //     {
            //         key: 'types',
            //         tag: 'MgsICheckGroup',
            //         props: {
            //             checkAll: true,
            //             items: [
            //                 {
            //                     label: 'type1',
            //                     content: '类型1',
            //                 },
            //                 {
            //                     label: 'type2',
            //                     content: '类型2',
            //                 },
            //                 {
            //                     label: 'type3',
            //                     content: '类型3',
            //                 },
            //             ],
            //         },
            //     },
            //     {
            //         key: 'graphic',
            //         tag: 'MgsIMapDraw',
            //         props: {
            //             bindMap: null,
            //             items: [
            //                 {
            //                     type: 'Circle',
            //                     name: '半径检索',
            //                 },
            //                 {
            //                     type: 'Rectangle',
            //                     name: '范围检索',
            //                 },
            //             ],
            //         },
            //     },
            // ],
        }),
        events: [
            {
                key: 'search',
                receivers: [
                    {
                        id: 'layout-01',
                        key: 'loadModule',
                        sync: true,
                        data: ['comp-04'],
                    },
                    { id: 'comp-04', key: 'queryReceiver' },
                ],
            },
        ],
        lyConfig: {
            type: 'float',
            position: ['top', 'left'],
            style: { width: '350px' },
        },
    },
    {
        id: 'control-05',
        name: 'MgsToolbarV1',
        type: 'component',
        props: [
            bindMapProp,
            ...buildConstantProps({
                items: [
                    {
                        toolname: '测量',
                        vuepath: 'Measure',
                        exectype: 2,
                        toolicon: 'jhcl',
                    },
                    {
                        toolname: '地形分析',
                        vuepath: 'MapAnalysis',
                        exectype: 2,
                        toolicon: 'swfx',
                    },
                    {
                        toolname: '移动地图',
                        vuepath: 'Pan',
                        begingroup: 1,
                        exectype: 1,
                        toolicon: 'yddt',
                    },
                    {
                        toolname: '重置视图',
                        vuepath: 'ResetView',
                        exectype: 1,
                        toolicon: 'czst',
                        vueoptions: { view },
                    },
                    {
                        toolname: '坐标定位',
                        vuepath: 'Locate',
                        exectype: 3,
                        toolicon: 'zbdw',
                        customprops: {
                            width: '300px',
                        },
                    },
                    {
                        toolname: '打印',
                        vuepath: 'BaseTool',
                        exectype: 2,
                        toolicon: 'dtdy',
                        vueoptions: {
                            items: [
                                {
                                    name: '屏幕打印',
                                    comp: 'CutScreen',
                                    dialogProps: {
                                        fullscreen: true,
                                        viewButtons: true,
                                    },
                                },
                                {
                                    name: '专业打印',
                                    comp: 'Print',
                                },
                            ],
                        },
                    },
                    {
                        toolname: '对比',
                        vuepath: 'BaseTool',
                        exectype: 2,
                        toolicon: 'jldb',
                        vueoptions: {
                            items: [
                                {
                                    name: '卷帘对比',
                                    comp: 'ViewCompare',
                                    props: {
                                        options: mapOptions,
                                        compFir: 'MgsServiceCatalogView',
                                        compSec: 'MgsServiceCatalogView',
                                    },
                                    dialogProps: { fullscreen: true },
                                },
                                {
                                    name: '分屏对比',
                                    comp: 'ViewCompareV2',
                                    props: {
                                        options2d: mapOptions,
                                        compFir: 'MgsServiceCatalogView',
                                        compSec: 'MgsServiceCatalogView',
                                    },
                                    dialogProps: { fullscreen: true },
                                },
                            ],
                        },
                    },
                    {
                        toolname: '图层管理',
                        vuepath: 'tool-LayerManager',
                        exectype: 1,
                        toolicon: 'tcgl',
                    },
                    {
                        toolname: '清除',
                        begingroup: 1,
                        exectype: 0,
                        toolicon: 'qc',
                        onclick: function (preActive, bindMap, vm) {
                            vm.toolbar.$emit('clearAll');
                        },
                    },
                ],
                getIcon: function (item, hover) {
                    const suffix = hover ? '_pre' : '';
                    return `./img/toolbar/tool_${item.toolicon}${suffix}.png`;
                },
            }),
        ],
        events: [
            {
                key: 'analysis',
                receivers: [{ id: 'control-09', key: 'show' }],
            },
            {
                key: 'clearAll',
                receivers: [{ id: 'control-09', key: 'close' }],
            },
        ],
        children: [
            {
                id: 'tool-LayerManager',
                name: 'LayerManager',
                type: 'component',
                props: [
                    ...buildComputedProps({
                        baseLayers: 'control-01 baseLayers',
                    }),
                    ...buildConstantProps({
                        style: { top: '60px', right: '15px' },
                    }),
                ],
            },
        ],
        lyConfig: {
            type: 'float',
            position: ['top', 'right'],
        },
    },
    {
        id: 'control-06',
        name: 'MgsPosScale',
        type: 'component',
        props: [bindMapProp],
        lyConfig: {
            type: 'float',
            position: ['bottom', 'left'],
        },
    },
    {
        id: 'control-07',
        name: 'MgsScale',
        type: 'component',
        props: [bindMapProp],
        lyConfig: {
            type: 'float',
            position: ['bottom', 'left'],
            offset: [50],
        },
    },
    {
        id: 'control-08',
        name: 'Mgs3dNav',
        type: 'component',
        props: [bindMapProp],
        lyConfig: {
            type: 'float',
            position: ['bottom', 'right'],
            offset: [200, -15],
        },
    },
    {
        id: 'control-09',
        name: 'Legend',
        type: 'component',
        props: [bindMapProp],
        lyConfig: {
            type: 'float',
            position: ['bottom', 'right'],
            offset: [55, 70],
        },
    },
    /* ***** 其他组件类型 ***** */
    {
        id: 'other-01',
        name: 'MgsTrackPlayer',
        type: 'component',
        props: [
            bindMapProp,
            ...buildConstantProps({ style: { right: '15px', top: '60px' } }),
        ],
    },
];

const compConfigs = {};
componentsConfig.forEach(c => {
    compConfigs[c.id] = c.lyConfig || {};
    delete c.lyConfig;
});
// 第一个组件（一般为布局组件）
const layoutConfig = {
    id: 'layout-01',
    name: 'MgsLyOnlinemap',
    type: 'layout',
    props: buildConstantProps({
        // 额外的组件配置（提供给布局组件使用）
        compConfigs: compConfigs,
    }),
    events: [
        {
            key: 'module-change',
            // 自定义事件demo
            callback(evt) {
                const { args, sender } = evt;

                // 参数
                const [v1, v2] = args;
                const { engin } = sender;

                // 传递事件
                v2 && engin.postMessage(v2, 'deactivated');
                v1 && engin.postMessage(v1, 'activated');
            },
        },
    ],
};

export default {
    id: 'scheme-01',
    name: '作战地图',
    description: '这是作战地图应用！',
    entry: 'layout-01',
    components: [layoutConfig, ...componentsConfig],
};
