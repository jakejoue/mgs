/*
 * @Author: 黄威
 * @Date: 2021-11-23 10:19:31
 * @LastEditors: wanghan
 * @LastEditTime: 2022-01-14 14:04:51
 * @Description: view 模块化入口
 */

// 业务组件
import MgsServiceCatalogView from './mgs-service-catalog-view.vue';
import MgsPathNavView from '@mgs/components/mgs-path-nav';
import MgsGeocodingView from './mgs-geocoding-view.vue';
import MgsPlottingPlanView from './mgs-plotting-plan-view.vue';
import MgsBookmarkView from './mgs-bookmark-view.vue';

export default {
    install(Vue) {
        Vue.component('MgsServiceCatalogView', MgsServiceCatalogView);
        Vue.component('MgsPathNavView', MgsPathNavView);
        Vue.component('MgsGeocodingView', MgsGeocodingView);
        Vue.component('MgsPlottingPlanView', MgsPlottingPlanView);
        Vue.component('MgsBookmarkView', MgsBookmarkView);
    },
};
