/*
 * @Author: 黄威
 * @Date: 2021-12-13 14:54:06
 * @LastEditors: 黄威
 * @LastEditTime: 2022-01-25 16:06:07
 * @Description: 工具项入口
 */

import BaseTool from './BaseTool.vue';
import Measure from './Measure.vue';
import MapAnalysis from './MapAnalysis.vue';
import Pan from './Pan.vue';
import LayerManager from './LayerManager.vue';
import ViewCompare from './ViewCompare.vue';
import ViewCompareV2 from './ViewCompareV2.vue';
import Identify from './Identify.vue';
import DataQuery from './DataQuery.vue';
import ImportFile from './ImportFile.vue';
import DataAnalysis from './DataAnalysis.vue';
import CutScreen from './CutScreen.vue';
import Locate from './Locate.vue';
import ResetView from './ResetView.vue';
import Print from './Print.vue';

const components = {
    BaseTool,
    Measure,
    MapAnalysis,
    Pan,
    LayerManager,
    ViewCompare,
    ViewCompareV2,
    Identify,
    DataQuery,
    ImportFile,
    DataAnalysis,
    CutScreen,
    Locate,
    ResetView,
    Print,
};

export default {
    // 注册工具组件
    install(Vue) {
        for (const key in components) {
            Vue.component(key, components[key]);
        }
    },
    // 目前支持的组件
    ...components,
};
