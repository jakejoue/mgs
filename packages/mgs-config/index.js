/*
 * @Author: 黄威
 * @Date: 2021-11-23 17:05:28
 * @LastEditors: 黄威
 * @LastEditTime: 2022-02-07 16:08:00
 * @Description: 一些请求ip配置（组件内部会使用相关请求方法）
 */
import { extend, pick } from '@mgs/utils/misc';

export default {
    // api查询前缀配置
    API_URLS: {},

    // token验证配置
    CHECK_TOKEN_URL: '',

    // hdfs相关配置
    IMG_URL: '',
    IMG_AFTER_IMGURL: '',

    install(Vue, options = {}) {
        extend(
            this,
            pick(options, [
                'API_URLS',
                'CHECK_TOKEN_URL',
                'IMG_URL',
                'IMG_AFTER_IMGURL',
            ])
        );
    },
};
