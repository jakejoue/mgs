import Vue from 'vue';

// 配置
type FormItem = {
    // 每个item的类名（用于自定义样式）
    class?: string;
    // 数据对应的键值
    prop?: string;
    // 数据 label 说明
    label?: string;
    // 默认宽度
    span?: number;
    
    // label宽度
    labelWidth?: string;
    // 类型
    required?: boolean;
    // 验证项目
    rule?: any;
    // 自定义插槽
    slot?: string;
    // 类别（用于控制渲染）
    type?: string;

    // 数据，默认值
    value?: any;
    // 选项集合（用于selects 和 radiogroup相关带子选项的栏目）
    options?: { value: string; label: string }[];
    // 异步加载options方法
    loadOptions?: (callback: (options: any) => void) => void;

    // 属性
    props?: { [key: string]: any };
    // 事件
    events?: { [key: string]: any };
    // 子项目
    items?: FormItem[];
};

export class MgsFormCreator extends Vue {
    // 表单数据
    formData?: Object;

    // 表单构造项目
    items: FormItem[];

    // 默认宽度 默认24
    defSpan?: Number;

    // label宽度 默认7em
    labelWidth?: String;

    // 表单大小
    size?: 'medium' | 'small' | 'mini';

    // 只读 false
    readonly?: Boolean;

    // 链式属性 false
    chainProp?: Boolean;

    // 分组内容间隔（处理header间隔）
    padding?: String;
}
