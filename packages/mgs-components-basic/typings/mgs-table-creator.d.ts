import { ElTableColumn } from 'element-ui/types/table-column';

export interface MgsTableColumn extends ElTableColumn {
    // 自定义内容
    slot?: string;
    comp?: string;

    // 子列（多级表头）
    children?: MgsTableColumn[];
}
