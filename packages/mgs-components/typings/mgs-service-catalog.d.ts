import Vue from 'vue';
import { BindMap, ServiceType } from 'mgs-webui-basic-map';

// 资源目录树节点
export interface CatalogTreeNode {
    // 节点id
    id: string;
    // 标题
    label: string;
    // 节点属性信息（存放图层等相关信息）
    props: {
        // 节点类型 服务 或 图层 或 目录
        nodetype?: 's' | 'l' | 'c';
        // 服务类型
        servicetype: ServiceType;
        // 服务地址
        serviceurl: string;
        // 附加的配置项 JSONSTRING '{key: value, ...}'
        servicestyles?: string;
        // 图例信息 JSONString LegendItem[]
        legendcontent?: string | LegendItem[];
        // 服务元数据
        servicemetadata?: any;
    };
    // 子节点信息
    children?: CatalogTreeNode[];
}

// 图例
export interface LegendItem {
    // 图例分组
    groupName: string;

    // 图例名称
    itemName: string;
    // 图例样式
    itemStyle?: string;
    // 图例图片
    imageUrl?: string;
}

export class MgsServiceCatalog extends Vue {
    /**
     * 绑定的地图对象
     */
    bindMap?: BindMap;
    /**
     * 加载状态 默认 false
     */
    loading?: Boolean;
    /**
     * 获取icon
     */
    getIcon?: (nodetype: any, node: CatalogTreeNode) => string;
    /**
     * 节点数据
     */
    data?: CatalogTreeNode[];
}

export class MgsLegend extends Vue {
    /**
     * 图例内容
     */
    content?: LegendItem[];
    /**
     * 图例图片基础路径，默认 ''
     */
    picStore?: string;
}
