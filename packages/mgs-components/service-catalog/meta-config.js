/*
 * @Author: 黄威
 * @Date: 2021-10-19 11:44:14
 * @LastEditors: 黄威
 * @LastEditTime: 2021-12-22 17:28:18
 * @Description: 元数据模板
 */

export default [
    {
        label: '服务标识',
        prop: 'service_identify',
        span: 24,
        items: [
            {
                label: '资源ID',
                prop: 'meta_resourceid',
            },
            {
                label: '资源名称',
                prop: 'meta_sresourcename',
            },
            {
                label: '服务ID',
                prop: 'meta_serviceid',
            },
            {
                label: '服务名称',
                prop: 'meta_sname',
            },
            {
                label: '场景名称',
                prop: 'meta_sscenename',
            },
            {
                label: '图层名称',
                prop: 'meta_slayername',
            },
            {
                label: '图层类型',
                prop: 'meta_slayertype',
            },
            {
                label: '数据集ID',
                prop: 'meta_sdatasetid',
            },
            {
                label: '数据集名称',
                prop: 'meta_sdatasetname',
            },
            {
                label: '数据集类型',
                prop: 'meta_sdatasettype',
            },
            {
                label: '服务版本',
                prop: 'meta_sversion',
            },
            {
                label: '服务类型',
                prop: 'meta_stype',
            },
            {
                label: '接口类型',
                prop: 'meta_interface',
            },
            {
                label: '摘要',
                prop: 'meta_sremark',
            },
            {
                label: '关键词',
                prop: 'meta_skeys',
            },
            {
                label: '描述',
                prop: 'meta_range',
            },
            {
                label: '服务访问地址',
                prop: 'meta_surl',
                span: 24,
                type: 'textarea',
            },
        ],
    },
    {
        label: '空间信息',
        prop: 'service_spatialinfo',
        span: 24,
        items: [
            {
                label: '坐标系',
                prop: 'service_prjcoordsys',
                items: [
                    {
                        label: 'EPSGCode',
                        prop: 'prjcoordsyscode',
                    },
                    {
                        label: '名称',
                        prop: 'prjcoordsysName',
                    },
                ],
            },
            {
                label: '坐标范围',
                prop: 'service_boundingbox',
                items: [
                    {
                        label: '左',
                        prop: 'bounds_left',
                    },
                    {
                        label: '上',
                        prop: 'bounds_top',
                    },
                    {
                        label: '右',
                        prop: 'bounds_right',
                    },
                    {
                        label: '下',
                        prop: 'bounds_bottom',
                    },
                ],
            },
            {
                label: '中心点',
                prop: 'service_center',
                items: [
                    {
                        label: 'X',
                        prop: 'center_x',
                    },
                    {
                        label: 'Y',
                        prop: 'center_y',
                    },
                ],
            },
        ],
    },
    {
        label: '更新周期',
        prop: 'update_cyc',
        span: 24,
        items: [
            {
                label: '生产时间',
                prop: 'production_sdate',
            },
            {
                label: '更新时间',
                prop: 'update_sdate',
            },
            {
                label: '更新周期',
                prop: 'update_cycle',
            },
        ],
    },
];
