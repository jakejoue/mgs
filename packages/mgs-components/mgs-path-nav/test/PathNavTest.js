const navData = require('./data/pathnavdata_v2.json').data;

const { pathGuideItemTurnTypes, pathGuides, pathGuideItemWeights, geoLineMs } =
    navData;

// 方向信息
const turnTypes = pathGuideItemTurnTypes
    .slice(1, -1)
    .split(', ')
    .map(item => item.split('=')[1]);

// 路线信息
pathGuides.length = Object.keys(pathGuides).length;
const guides = Array.from(pathGuides);

// 权重信息（可以由此得到线路信息）
pathGuideItemWeights.length = Object.keys(pathGuideItemWeights).length;
const weights = Array.from(pathGuideItemWeights);

// 有效的线路信息
const linesMessage = [];
let preWeight = 0;

for (let i = 0; i < weights.length; i++) {
    const weight = weights[i];

    if (weight > 0) {
        preWeight += weight;

        linesMessage.push({
            // 线段名称和转向
            guide: guides[i],
            turnType: turnTypes[i - 1],
            // 权重信息
            weight: weight,
            totalWeight: preWeight,
        });
    }
}

export default {
    // 总距离
    distance: preWeight,
    // 线段信息
    linesMessage,
    // 点信息
    geoLineMs,
};
