/*
 * @Author: 黄威
 * @Date: 2022-01-04 14:07:19
 * @LastEditors: 黄威
 * @LastEditTime: 2022-01-05 10:03:27
 * @Description: 网格布局构造器
 */

import Vue, { Component } from 'vue';

/**
 * 网格配置
 * @example
 * var grid = {
 *   tag: "el-container",
 *   children: [
 *       {
 *           tag: "el-aside",
 *       },
 *       {
 *           tag: "el-container",
 *           children: [
 *               {
 *                   tag: "el-header",
 *               },
 *               {
 *                   tag: "el-main",
 *               },
 *               {
 *                   tag: "el-footer",
 *               },
 *           ],
 *       },
 *   ],
 * };
 */
export type Grid = {
    // 节点类型
    tag?: string | Component;

    // 内容部分（展示自定义插槽或者组件）
    slot?: string;
    comp?: string;

    // 节点绑定的属性
    props?: { [key: string]: any };

    // 子节点
    children?: Grid[];
};
