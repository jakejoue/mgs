import Vue from 'vue';
import { BindMap, ServiceType } from 'mgs-webui-basic-map';

// 地图方案
export interface BaseMap {
    // 标题
    title: string;

    // 方案图片位置
    thumbnail: string;

    // 下面绑定的服务
    services: [
        {
            // 服务id
            serviceid: string;
            // 服务名称
            servicename?: string;
            // 服务类型
            servicetype: ServiceType;
            // 服务地址
            serviceurl: string;
            // 附加的配置项 JSONSTRING '{key: value, ...}'
            servicestyles?: string;
        }
    ];
}

export class MgsBasemapSwitch extends Vue {
    /**
     * 绑定的地图对象
     */
    bindMap?: BindMap;

    /**
     * 绑定的底图方案
     */
    baseMaps?: Array<BaseMap>;
}
