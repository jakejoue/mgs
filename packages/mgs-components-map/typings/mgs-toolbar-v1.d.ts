import Vue from 'vue';
import { BindMap } from 'mgs-webui-basic-map';

// 工具栏数据类型
export interface ToolBarItem {
    /**
     * 四种类型：
     *
     * 0（调用配置的onclick事件）
     * 1（exec，点击执行对应组件的exec）
     * 2（popover点击展开弹框）
     * 3（dialog点击展开对话框）
     */
    exectype: 0 | 1 | 2 | 3;

    // 名称
    toolname: string;

    /* 如果指定 getIcon，下面的icon都是可选的 */
    // icon名称
    toolicon?: string;
    // 选中下的icon
    tooliconhover?: string;

    // 分组（判断是否需要风格线）1 或 0
    begingroup?: number;

    // 点击事件（用于simple类型，如果存在点击事件则尝试运行）
    onclick?: (preActive: boolean, vue: any) => void;

    // 构造item用到的属性（用于存放 弹出框 和 popover 的宽度等信息）
    // popover 模式数据 eg: `{ width: '300px' }`
    // dialog 模式数据 eg: `{ width: '300px', viewButtons: true }`
    customprops?: string | object;

    // 组件路径
    vuepath?: string | object | (() => any);

    // 构造vm示例的参数，用于指定的vuepath组件访问使用（string类型会尝试转为对象）
    vueoptions?: string | object;
}

export class MgsToolbarV1 extends Vue {
    /**
     * 工具栏方向 默认 horizontal
     */
    orientation?: 'horizontal' | 'vertical';

    /**
     * 工具栏元素
     */
    items?: Array<ToolBarItem>;

    /**
     * 获取工具栏icon
     */
    getIcon?: (item: ToolBarItem, hover: boolean) => string;

    /**
     * 绑定的地图
     */
    bindMap?: BindMap;
}
