/*
 * @Author: huangwei
 * @Date: 2021-05-13 17:45:51
 * @LastEditors: huangwei
 * @LastEditTime: 2021-05-13 18:11:02
 * @Description: 结果集入口
 */

import MgsSearchResult from './mgs-search-result.vue';
export default MgsSearchResult;
