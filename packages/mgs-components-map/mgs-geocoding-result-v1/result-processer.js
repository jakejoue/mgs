/*
 * @Author: huangwei
 * @Date: 2021-05-18 11:09:02
 * @LastEditors: 黄威
 * @LastEditTime: 2021-12-27 21:58:11
 * @Description: 根据配置进行结果处理
 */

import centroid from '@turf/centroid';

class Processer {
    constructor(fieldConfig) {
        this.fieldConfig = fieldConfig;
    }

    // icon处理
    getIcon(data, select = false) {
        const icon_field = this.fieldConfig.icon_field;
        const icon = data[icon_field] || './img/pos.png';

        if (select) {
            const dotIdx = icon.lastIndexOf('.');
            const iconPrefix = icon.slice(0, dotIdx);
            const iconSuffix = icon.slice(dotIdx);
            return iconPrefix + `_sel` + iconSuffix;
        }

        return icon;
    }

    // 标题处理
    getTitle(data) {
        const title_field = this.fieldConfig.title_field;

        if (!title_field) return;
        return data[title_field];
    }

    // 内容处理
    getContent(data) {
        const view_fields = this.fieldConfig.view_fields;

        const dataArr = [];
        for (const key in view_fields) {
            const name = view_fields[key];

            dataArr.push({
                title: name,
                content: data[key] || '',
            });
        }
        return dataArr;
    }

    // 获取多边形
    getGeometry(data) {
        const geom_field = this.fieldConfig.geom_field;

        return data[geom_field];
    }

    // 获取中心点
    geoPos(data) {
        const pos_field = this.fieldConfig.pos_field;

        // 获取中心点
        const latLng = data[pos_field];
        if (latLng) {
            return {
                type: 'Point',
                coordinates: [
                    isNaN(+latLng.lng) ? latLng.lon : latLng.lng,
                    latLng.lat,
                ],
            };
        }

        // 从多边形中获取中心点
        const geom = this.getGeometry(data);
        if (geom) {
            return centroid(geom).geometry;
        }
        return;
    }

    // 检查配置是否满足要求
    check(data) {
        const { data_type, data_type_field } = this.fieldConfig;
        return data[data_type_field] === data_type;
    }

    // 数据转换
    parse(data) {
        return {
            meta_data: data,
            geom: this.getGeometry(data),
            pos: this.geoPos(data),
            icon: this.getIcon(data),
            icon_sel: this.getIcon(data, true),
            title: this.getTitle(data),
            content: this.getContent(data),
        };
    }
}

export default function getProcesser(fieldConfigs = []) {
    const processes = fieldConfigs.map(fc => new Processer(fc));

    return data => {
        const ps = processes.find(p => p.check(data)) || processes[0];
        return ps && ps?.parse(data);
    };
}
