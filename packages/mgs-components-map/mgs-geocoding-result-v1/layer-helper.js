/*
 * @Author: huangwei
 * @Date: 2021-05-18 17:01:00
 * @LastEditors: 黄威
 * @LastEditTime: 2021-12-27 21:37:59
 * @Description: 图层构造器（用于实现结果和地图联动）
 */
// 绑定的事件
const events = {
    click(evt) {
        const g = evt.graphics[0] || { __id__: -1 };
        this.setSelectIdx(g.__id__);
        this.onClick && this.onClick(g.__id__);
    },
    mouseover(evt) {
        const g = evt.graphics[0] || { __id__: -1 };
        this.setHoverIdx(g.__id__);
        this.onHover && this.onHover(g.__id__);
    },
    mouseout() {
        this.setHoverIdx(-1);
        this.onHover && this.onHover(-1);
    },
};

export default class ResultLayer {
    // 地图和图层
    // bindMap?
    // layer?

    // 多边形结果
    targetMap = new Map();

    // 选中的索引
    selectIdx = -1;
    hoverIdx = -1;

    // 事件绑定
    // onHover?: (index) => void;
    // onClick?: (index) => void;

    constructor() {}

    // 设置结果集
    setResults(val = [], fitBound = true) {
        // 清空图层
        this.layer.clear();
        this.targetMap.clear();

        // 还原参数
        this.selectIdx = -1;
        this.hoverIdx = -1;

        if (val && val.length) {
            const boundary = [180, 90, -180, -90];

            // 添加到地图中
            for (let i = 0; i < val.length; i++) {
                const item = val[i];
                // 不存在位置信息的时候
                if (!item.pos) continue;

                try {
                    const coord = item.pos.coordinates;

                    // 存在位置信息的时候
                    this.addItemToLayer(item, i);

                    // 扩展边界
                    boundary[0] = Math.min(boundary[0], coord[0]);
                    boundary[1] = Math.min(boundary[1], coord[1]);
                    boundary[2] = Math.max(boundary[2], coord[0]);
                    boundary[3] = Math.max(boundary[3], coord[1]);
                } catch (error) {
                    // 坐标值无效（出错）
                    continue;
                }
            }

            // 如果不需要定位
            if (!fitBound) return;

            // 定位到结果集
            if (boundary[0] === boundary[2] && boundary[1] === boundary[3]) {
                this.bindMap.zoom2Centoid(boundary);
            } else {
                this.bindMap.zoom2Extent(boundary);
            }
        }
    }

    // 设置选中索引号
    setSelectIdx(index) {
        if (this.selectIdx === index) return;

        const ovl = this.targetMap.get(this.selectIdx);
        const nvl = this.targetMap.get(index);

        ovl && this.resetItem(ovl);
        nvl && this.highLightItem(nvl);

        this.selectIdx = index;
    }

    setHoverIdx(index) {
        if (this.hoverIdx === index) return;

        const ovl = this.targetMap.get(this.hoverIdx);
        const nvl = this.targetMap.get(index);

        if (
            this.selectIdx < 0 ||
            (this.selectIdx >= 0 && this.selectIdx !== this.hoverIdx)
        ) {
            ovl && this.resetItem(ovl);
        }
        nvl && this.highLightItem(nvl);

        this.hoverIdx = index;
    }

    // 用于添加和销毁的接口
    setMap(bindMap) {
        // 初始化
        if (bindMap && !this.bindMap) {
            this.bindMap = bindMap;
            this.layer = this.bindMap.newGraphicLayer();

            this.bindEvents(bindMap);
        }
        // 销毁
        else if (!bindMap && this.bindMap) {
            this.setResults();
            this.layer.destroy();
            this.bindEvents(this.bindMap, true);
            this.bindMap = undefined;
            this.layer = undefined;
            this.onClick = this.onHover = undefined;
        }
    }

    addItemToLayer(item, index) {
        const geoType = item.geom.type;

        const gs = [];

        const icon = this.bindMap.drawGraphic(
            'Marker',
            item.pos.coordinates,
            {
                img: item.icon,
                disableDepthTestDistance: 250000,
            },
            null,
            this.layer
        );

        icon.__icon = item.icon;
        icon.__icon_sel = item.icon_sel;

        const num = this.bindMap.drawGraphic(
            'Label',
            item.pos.coordinates,
            {
                text: String(index + 1),
                offset: [0, 5],
                fontColor: '#fff',
                disableDepthTestDistance: 250000,
            },
            null,
            this.layer
        );

        gs.push(icon, num);

        if (geoType !== 'Point') {
            const gemo = this.bindMap.drawGraphic(
                geoType === 'LineString' ? 'Polyline' : 'Polygon',
                item.geom.coordinates,
                null,
                null,
                this.layer
            );
            gs.push(gemo);
        }

        gs.forEach(g => (g.__id__ = index));
        this.targetMap.set(index, gs);
    }

    highLightItem(target) {
        const [icon, , geom] = target;

        icon.setStyle({
            img: icon.__icon_sel,
            disableDepthTestDistance: 250000,
        });
        geom && geom.setStyle({ color: 'red', fillColor: 'red' });
    }

    resetItem(target) {
        const [icon, , geom] = target;

        icon.setStyle({
            img: icon.__icon,
            disableDepthTestDistance: 250000,
        });
        geom && geom.setStyle({});
    }

    panToIdx(index) {
        const target = this.targetMap.get(index);
        if (!target) return;

        this.bindMap.zoom2Geometry(target[0].feature);
    }

    bindEvents(bindMap, unBind = false) {
        for (const eventName in events) {
            bindMap.selectHandler[unBind ? 'off' : 'on'](
                eventName,
                this.layer,
                events[eventName],
                this
            );
        }
    }
}
