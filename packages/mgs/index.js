/*
 * @Author: 黄威
 * @Date: 2021-11-18 17:24:21
 * @LastEditors: 黄威
 * @LastEditTime: 2021-12-30 15:39:23
 * @Description: 全部出口
 */

// 配置管理（一些平台配置相关）
import mgsConfig from '@mgs/config';
// Vue 常用指令插件
import mgsPlugins from '@mgs/plugins';
// 核心渲染库
import mgsCore from '@mgs/core';

export default {
    install(Vue, config) {
        Vue.use(mgsConfig, config);
        Vue.use(mgsPlugins);
        Vue.use(mgsCore);
    },
};
