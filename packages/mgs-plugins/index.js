// 自定义指令
import clickoutside from './directives/clickoutside';
import drag from './directives/drag';
import dom from './directives/dom';

import emitter from './mixins/emitter';

export { clickoutside, drag, dom, emitter };

export default {
    install(Vue) {
        Vue.directive('clickoutside', clickoutside);
        Vue.directive('drag', drag);
    },
};
