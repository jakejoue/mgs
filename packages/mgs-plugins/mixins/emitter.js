/*
 * @Author: 黄威
 * @Date: 2021-12-02 17:38:58
 * @LastEditors: 黄威
 * @LastEditTime: 2021-12-02 17:41:14
 * @Description: 父子联动器
 */

function broadcast(componentName, eventName, params) {
    this.$children.forEach(child => {
        const name = child.$options.name;

        if (name === componentName) {
            child.$emit.apply(child, [eventName].concat(params));
        } else {
            // 如果 params 是空数组，接收到的会是 undefined
            broadcast.apply(child, [componentName, eventName].concat([params]));
        }
    });
}

export default {
    methods: {
        dispatch(componentName, eventName, params) {
            let parent = this.$parent || this.$root;
            let name = parent.$options.name;

            while (parent && (!name || name !== componentName)) {
                parent = parent.$parent;

                if (parent) {
                    name = parent.$options.name;
                }
            }
            if (parent) {
                // @ts-ignore
                // eslint-disable-next-line prefer-spread
                parent.$emit.apply(parent, [eventName].concat(params));
            }
        },

        broadcast(componentName, eventName, params) {
            broadcast.call(this, componentName, eventName, params);
        },
    },
};
