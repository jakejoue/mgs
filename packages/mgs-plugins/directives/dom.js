/*
 * @Author: 黄威
 * @Date: 2022-01-07 09:48:17
 * @LastEditors: 黄威
 * @LastEditTime: 2022-01-07 18:23:29
 * @Description: 插入dom节点指令
 */

/**
 * 更新dom内容
 * @param {Element} el
 * @param {String | Element} value
 */
function updateContent(el, value) {
    value = value || '';

    if (typeof value === 'string') {
        el.innerHTML = value;
    } else {
        el.innerHTML = '';
        el.appendChild(value);
    }
}

export default {
    bind(el, { value }) {
        updateContent(el, value);
    },

    update(el, { value, oldValue }) {
        if (value === oldValue) return;
        updateContent(el, value);
    },

    unbind(el, binding, vnode, oldVnode, isDestroy) {
        if (!isDestroy) {
            updateContent(el);
        }
    },
};
