/*
 * @Author: 黄威
 * @Date: 2021-12-02 17:38:58
 * @LastEditors: 黄威
 * @LastEditTime: 2021-12-09 15:07:06
 * @Description: 点击到dom对象外
 */

export default {
    bind(el, binding) {
        function documentHandler(e) {
            if (el.contains(e.target)) {
                return false;
            }
            if (binding.expression) {
                binding.value(e);
            }
        }
        el.__vueClickOutside__ = documentHandler;
        document.addEventListener('click', documentHandler);
    },
    unbind(el) {
        document.removeEventListener('click', el.__vueClickOutside__);
        delete el.__vueClickOutside__;
    },
};
