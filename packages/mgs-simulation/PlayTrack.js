/*
 * @Author: 黄威
 * @Date: 2021-12-28 09:59:11
 * @LastEditors: 黄威
 * @LastEditTime: 2022-01-06 13:57:53
 * @Description: 轨迹播放
 */

import { requestFn, cancelFn } from '@mgs/utils/misc';

/**
 * 构造轨迹点参数
 * @typedef {{x:String, y:String, weight:String}} TickOptions
 *
 * 定时器参数定义
 * @typedef {{tickLen: Number, speed: Number, callback: (p:TickPoint)=>void }} ClockOptions
 */

// 轨迹点
class TickPoint {
    /**
     * 轨迹点
     * @param {Object} item 原始数据
     * @param {TickOptions} option 构造参数
     */
    constructor(item, option = {}) {
        option = { x: 'x', y: 'y', weight: 'weight', ...option };

        // 支持创建一个空对象
        if (item) {
            this.item = item;

            /** @type {Number} */
            this.x = item[option.x];
            /** @type {Number} */
            this.y = item[option.y];
            /** @type {Number} */
            this.w = item[option.weight];
        }

        /** @type {TickPoint} 上个点 */
        this.pre = null;
        /** @type {TickPoint} 下个点 */
        this.next = null;
    }

    /**
     * 获取当前朝向
     * @returns {Number}
     */
    get bearing() {
        if (!this.pre) return 0;
        return 1;
    }

    /**
     * @private
     * 求指定百分比的点
     * @param {Number} ratio 百分比
     */
    _interpolate(ratio) {
        if (ratio === 1) return this.next || this;
        if (ratio === 0) return this;

        const p = new TickPoint();
        const p1 = this;
        const p2 = this.next;

        p.x = p1.x + (p2.x - p1.x) * ratio;
        p.y = p1.y + (p2.y - p1.y) * ratio;
        p.w = p1.w + (p2.w - p1.w) * ratio;

        // 构造链式属性
        p.pre = p1.pre || p1;
        p.next = p2;

        return p;
    }

    /**
     * @public
     * 求通过权重的点位，用于计算播放时候的当前位置点
     * @param {Number} w
     * @returns {TickPoint}
     */
    tick(w) {
        const startW = this.w;

        // 计算上个点
        if (w < startW) {
            if (this.pre) return this.pre.tick(w);
            return this;
        }

        // 计算下个点
        if (!this.next) return this;

        // 跳过到下个点
        const endW = this.next.w;
        if (w > endW) return this.next.tick(w);

        // 计算中间点
        return this._interpolate((w - startW) / (endW - startW));
    }
}

// 定时器
class TickClock {
    /**
     * 轨迹播放
     * @param {ClockOptions} options
     */
    constructor(options) {
        this._interval = null;
        this.setOptions({ tickLen: 1, speed: 1, ...options });

        // 是否正在播放
        this.isPlaying = false;

        /** @type {TickPoint} 当前点位 */
        this.curP = null;
    }

    /**
     * 设置参数
     * @param {ClockOptions} options
     */
    setOptions(options = {}) {
        options.tickLen && (this.tickLen = options.tickLen);
        options.speed && (this.speed = options.speed);
        options.callback && (this.callback = options.callback);
    }

    // 播放
    play() {
        // 正在播放中
        if (this.isPlaying) return;

        this.isPlaying = true;

        const self = this;
        const update = function () {
            // 如果存在控制点，并且控制点存在下个点
            if (self.isPlaying && self.curP && self.curP.next) {
                if (self.isPlaying) {
                    self._tick();
                    self.frame = requestFn(update);
                }
            } else {
                self.stop();
            }
        };

        update();
    }

    // 停止
    stop() {
        this.isPlaying = false;

        if (this.frame) {
            cancelFn(this.frame);
            delete this.frame;
        }
    }

    // 计算下个点
    _tick() {
        // 计算下个点
        const addW = this.tickLen * this.speed;
        this.curP = this.curP.tick(this.curP.w + addW);

        // 派发更新事件
        this.callback && this.callback(this.curP);
    }
}

// 轨迹播放
class PlayTrack extends TickClock {
    constructor(options) {
        super(options);

        /** @type {TickPoint[]} 轨迹点 */
        this.tickPoints = [];
    }

    /**
     * 设置轨迹数据
     * @param {Array} trackPoints 轨迹数据，数组
     * @param {TickOptions} options 参数 - 规定，x，y，weight 取值字段
     */
    setTrack(trackPoints, options) {
        const tickPoints = [];

        // 构造轨迹点
        for (let i = 0; i < trackPoints.length; i++) {
            const point = trackPoints[i];
            const p = new TickPoint(point, options);

            tickPoints[i] = p;

            // 构造链式属性
            if (tickPoints[i - 1]) {
                tickPoints[i - 1].next = p;
                p.pre = tickPoints[i - 1];
            }
        }

        // 赋值
        this.tickPoints = tickPoints;
        // 设置时钟 - 起始点
        this.curP = tickPoints[0];
    }

    // 最小权重
    get minW() {
        return this.tickPoints[0] ? this.tickPoints[0].w : 0;
    }
    // 最大权重
    get maxW() {
        const lastP = this.tickPoints[this.tickPoints.length - 1];
        return lastP ? lastP.w : 0;
    }

    // 跳转到指定权重
    setCursor(w) {
        if (this.curP) {
            this.curP = this.curP.tick(w);
            this.callback && this.callback(this.curP);
        }
    }
    // 获取当前已经播放的权重
    getCursor() {
        if (this.curP) {
            return this.curP.w;
        }
        return 0;
    }

    // 获取当前百分比
    getPercent() {
        const w = this.getCursor();
        const minW = this.minW;
        const maxW = this.maxW;

        const percent = (w - minW) / (maxW - minW);
        return Math.min(1, Math.max(percent, 0));
    }
    // 设置百分比
    setPercent(percent) {
        percent = Math.min(1, Math.max(percent, 0));

        const minW = this.minW;
        const maxW = this.maxW;

        const targetW = (maxW - minW) * percent + minW;
        this.setCursor(targetW);
    }

    // 重新播放
    rePlay() {
        this.setCursor(this.minW);
        this.play();
    }

    // 清空
    clear() {
        this.stop();
        this.curP = null;
        this.tickPoints = [];
    }
}

export default PlayTrack;
