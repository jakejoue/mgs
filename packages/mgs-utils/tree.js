/*
 * @Author: 黄威
 * @Date: 2021-12-22 17:01:33
 * @LastEditors: 黄威
 * @LastEditTime: 2022-03-02 10:37:25
 * @Description: 树结构数据操作工具类
 */

let uid_Index = 0;
function nextId() {
    return 'uid-' + ++uid_Index;
}

/**
 * 数组转成树结构
 * @param {*} arr
 * @param {TreeSetting} setting
 * @returns
 */
export function transformToTreeData(arr, setting) {
    const sNodes = [...(arr || [])];

    const r = [];
    const tmpMap = {};

    let i, l;
    for (i = 0, l = sNodes.length; i < l; i++) {
        // 转为 CatalogTreeNode结构类型
        sNodes[i] = {
            uid: nextId(),
            id: sNodes[i][setting.idKey],
            pid: sNodes[i][setting.parentKey],
            label: sNodes[i][setting.labelKey],
            props: sNodes[i],
        };

        tmpMap[sNodes[i].id] = sNodes[i];
    }

    for (i = 0, l = sNodes.length; i < l; i++) {
        // 父节点
        const p = tmpMap[sNodes[i].pid];

        if (p && sNodes[i].id != sNodes[i].pid) {
            let children = p.children;
            if (!children) {
                children = p.children = [];
            }
            // 存入children
            children.push(sNodes[i]);
        } else {
            r.push(sNodes[i]);
        }
    }

    return r;
}
