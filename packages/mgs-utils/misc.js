/*
 * @Author: 黄威
 * @Date: 2021-09-16 17:24:41
 * @LastEditors: 黄威
 * @LastEditTime: 2022-05-05 14:34:27
 * @Description: 一些常用的杂项方法
 */
import html2canvas from 'html2canvas';
import { Message } from 'element-ui';
import { error } from './log';

/* ----- 浏览器相关 ----- */

//获取地址栏传值
export function getQueryString(paramName) {
    const queryString = window.location.search.substr(1);
    const aPairs = queryString.split('&');

    for (let i = 0; i < aPairs.length; i++) {
        const aTmp = aPairs[i].split('=');
        if (aTmp[0] === paramName) {
            return aTmp[1];
        }
    }
    return '';
}

/**
 * 判断设备是不是移动端
 * @returns {Boolean}
 */
export function isMobile() {
    const flag = navigator.userAgent.match(
        /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i
    );
    return !!flag;
}

function getPrefixed(name) {
    return (
        window['webkit' + name] || window['moz' + name] || window['ms' + name]
    );
}

let lastTime = 0;

// fallback for IE 7-8
function timeoutDefer(fn) {
    const time = +new Date(),
        timeToCall = Math.max(0, 16 - (time - lastTime));

    lastTime = time + timeToCall;
    return window.setTimeout(fn, timeToCall);
}

export const requestFn =
    window.requestAnimationFrame ||
    getPrefixed('RequestAnimationFrame') ||
    timeoutDefer;

export const cancelFn =
    window.cancelAnimationFrame ||
    getPrefixed('CancelAnimationFrame') ||
    getPrefixed('CancelRequestAnimationFrame') ||
    function (id) {
        window.clearTimeout(id);
    };

// 赋值内容到剪切板
export function copyToClipboard(value, cb) {
    if (!value) {
        return;
    }
    const input = document.createElement('input');
    // input.style.display = 'none';
    document.body.appendChild(input);

    input.value = value;
    input.select();

    try {
        // 复制
        if (!document.execCommand('copy', false, null)) {
            throw '复制失败！';
        }
        Message.success('复制成功！');
        cb && cb();
    } catch (error) {
        Message.error('复制失败，请手动复制！');
    } finally {
        document.body.removeChild(input);
    }
}

// el转base64字符串
export async function elToBase64(el, width, height) {
    return new Promise((resolve, reject) => {
        const options = { useCORS: true, imageTimeout: 10000 };

        html2canvas(el, options)
            .then(canvas => {
                const url = canvas.toDataURL();

                // 如果指定宽高（二次压缩）
                if (width && height) {
                    const img = new Image();

                    img.src = url;
                    img.width = width;
                    img.height = height;
                    img.style = 'position: absolute;top: 0;z-index: -100;';
                    document.body.append(img);

                    img.onload = function () {
                        html2canvas(img, options)
                            .then(canvas2 => resolve(canvas2.toDataURL()))
                            .finally(() => img.remove());
                    };
                }
                // 输出打印的 base64 图片
                else {
                    resolve(url);
                }
            })
            .catch(reject);
    });
}
/* ----- 对象操作相关 ----- */

/**
 * typeOf
 * @param {Object} obj
 * @return {'boolean'|'number'|'string'|'function'|'array'|'date'|'regExp'|'undefined'|'null'|'object'}
 */
export function typeOf(obj) {
    const toString = Object.prototype.toString;
    const map = {
        '[object Boolean]': 'boolean',
        '[object Number]': 'number',
        '[object String]': 'string',
        '[object Function]': 'function',
        '[object Array]': 'array',
        '[object Date]': 'date',
        '[object RegExp]': 'regExp',
        '[object Undefined]': 'undefined',
        '[object Null]': 'null',
        '[object Object]': 'object',
    };
    return map[toString.call(obj)];
}

/**
 * 继承获取属性对象的属性值
 *
 * @param {Object} dest 目标对象
 * @param {Array<?Object>} sources 获取的属性的对象
 */
export function extend(dest, ...sources) {
    for (const src of sources) {
        for (const k in src) {
            dest[k] = src[k];
        }
    }
    return dest;
}

/**
 * 从源对象中挑选指定的属性并返回新的对象
 *
 * @param {Object} src 源对象
 * @param {Array<string>} properties 被挑选的属性数组
 * @returns {Object}
 * @example
 * const foo = { name: 'Charlie', age: 10 };
 * const justName = pick(foo, ['name']);
 * // justName = { name: 'Charlie' }
 */
export function pick(src, properties) {
    const result = {};
    for (let i = 0; i < properties.length; i++) {
        const k = properties[i];
        if (k in src) {
            result[k] = src[k];
        }
    }
    return result;
}

/**
 * 从源对象中排除指定的属性并返回新的对象
 *
 * @param {Object} src 源对象
 * @param {Array<string>} properties 要排除的属性数组
 * @returns {Object}
 * @example
 * const foo = { name: 'Charlie', age: 10 };
 * const justName = pick(foo, ['name']);
 * // justName = { age: 10 }
 */
export function exclude(src, properties) {
    const result = { ...src };
    for (let i = 0; i < properties.length; i++) {
        const k = properties[i];
        if (k in src) {
            delete result[k];
        }
    }
    return result;
}

/* ----- 字符串操作相关 ----- */

/**
 * 首字母转大写
 * @param {String} str 要转换的字符串
 * @param {Number} mode 模式 0 | 1 | 2
 * @returns {String}
 */
export function titleCase(str, mode = 0) {
    // 第一个字符大写，其他不变
    if (mode === 0) {
        const newStr = str.slice(0, 1).toUpperCase() + str.slice(1);
        return newStr;
    }
    // 第一个字符大写，其他转小写
    else if (mode === 1) {
        const newStr =
            str.slice(0, 1).toUpperCase() + str.slice(1).toLowerCase();
        return newStr;
    }
    // 空格拆分单词，每个单词第一个大写，其他转小写
    else if (mode === 2) {
        const newStr = str.split(' ');
        for (let i = 0; i < newStr.length; i++) {
            newStr[i] =
                newStr[i].slice(0, 1).toUpperCase() +
                newStr[i].slice(1).toLowerCase();
        }
        return newStr.join(' ');
    }
    // 不处理
    else {
        return str;
    }
}

/* ----- 一些杂项函数 ----- */

/**
 * @name: 米转化为千米、平方米转化为千米
 * @description: 大于1000米转化为千米，此外保留4位小数
 * @param {Number} value
 * @param {Boolean} isArea 计算距离或面积
 * @return {String}
 */
export function meterToKilometer(value, isArea = false) {
    let flag = false;
    let unit = '',
        p = 0;

    if (!isArea) {
        value > 1000 && (flag = true) && (value /= 1000);
        unit = flag ? 'km' : 'm';
        p = 2;
    } else {
        value > 1000000 && (flag = true) && (value /= 1000000);
        unit = flag ? 'k㎡' : '㎡';
        p = 4;
    }
    value = value.toFixed(p);

    return value + unit;
}

/**
 * 字符串转对象
 * @param {String|Object} data
 */
export function translateData(data) {
    if (typeof data === 'string') {
        try {
            return Function(`return ${data || '{}'};`)();
        } catch (err) {
            error('transferData', err);
        }
    }
    return data;
}

/**
 * 生成随机uuid
 */
export function createUuid() {
    const chars =
        '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split(
            ''
        );
    const uuid = [];
    let i;
    // var radix = chars.length
    let r;
    uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
    uuid[14] = '4';

    for (i = 0; i < 36; i++) {
        if (!uuid[i]) {
            r = 0 | (Math.random() * 16);
            uuid[i] = chars[i == 19 ? (r & 0x3) | 0x8 : r];
        }
    }
    return uuid.join('').replace(/-/g, '');
}

/* ----- 日期操作相关 ----- */

/**
 * 日期时间格式化
 * @param {*} fmt   格式表达式
 * @param {*} date  日期对象
 * @returns
 */
export function dateFormat(fmt, date) {
    let ret;
    const opt = {
        'Y+': date.getFullYear().toString(), // 年
        'm+': (date.getMonth() + 1).toString(), // 月
        'd+': date.getDate().toString(), // 日
        'H+': date.getHours().toString(), // 时
        'M+': date.getMinutes().toString(), // 分
        'S+': date.getSeconds().toString(), // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (const k in opt) {
        ret = new RegExp('(' + k + ')').exec(fmt);
        if (ret) {
            fmt = fmt.replace(
                ret[1],
                ret[1].length == 1
                    ? opt[k]
                    : opt[k].padStart(ret[1].length, '0')
            );
        }
    }
    return fmt;
}
