/*
 * @Author: 黄威
 * @Date: 2021-12-22 17:06:11
 * @LastEditors: 黄威
 * @LastEditTime: 2021-12-22 17:09:01
 * @Description: file content
 */

export interface TreeSetting {
    // id 字段
    idKey: string;
    // 父节点 字段
    parentKey: string;
    // label 字段
    labelKey: string;
}

declare type Node = {
    id: string;
    pid: string;
    label: string;
    props: any;
    children?: Node[];
};

/**
 * 数组转成树结构
 * @param arr 
 * @param setting 
 */
export function transformToTreeData(arr: [], setting: TreeSetting): Node[];
