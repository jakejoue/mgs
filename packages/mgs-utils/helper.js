/*
 * @Author: 黄威
 * @Date: 2021-12-09 15:03:51
 * @LastEditors: 黄威
 * @LastEditTime: 2022-01-07 14:24:24
 * @Description: 构造 mgs-app 配置项的工具类
 */

/**
 * 构造常量配置
 * @param {{[key:string]:any}} obj 原对象
 * @returns {Array}
 */
export function buildConstantProps(obj) {
    const props = [];
    for (const key in obj) {
        const val = obj[key];
        props.push({
            key: key,
            type: 'Constant',
            config: { type: typeof val, value: val },
        });
    }
    return props;
}

/**
 * 构造计算属性配置
 * @param {{[key:string]:string}} obj 原对象
 * @returns {Array}
 */
export function buildComputedProps(obj) {
    const props = [];
    for (const key in obj) {
        const val = obj[key];
        const [linkid, expression] = val.split(' ');

        props.push({
            key: key,
            type: 'Computed',
            config: {
                linkid: linkid,
                expression: expression,
            },
        });
    }
    return props;
}
