/*
 * @Author: 黄威
 * @Date: 2021-11-22 14:23:11
 * @LastEditors: 黄威
 * @LastEditTime: 2021-11-29 10:23:07
 * @Description: 用于输出日志的js
 */
const isDebug = process.env.NODE_ENV === 'development';

export function error(...rest) {
    console.error(...rest);
}

export function warn(...rest) {
    if (isDebug) {
        console.warn(...rest);
    }
}

export function log(...rest) {
    if (isDebug) {
        console.log(...rest);
    }
}
