/*
 * @Author: 黄威
 * @Date: 2021-09-18 14:59:45
 * @LastEditors: 黄威
 * @LastEditTime: 2021-12-16 14:29:43
 * @Description: 参数验证工具类，仿照Vue props的工具类
 */

const functionTypeCheckRE = /^\s*function (\w+)/;
const simpleCheckRE = /^(String|Number|Boolean|Function|Symbol|BigInt)$/;

function getType(fn) {
    const match = fn && fn.toString().match(functionTypeCheckRE);
    return match ? match[1] : '';
}

function isPlainObject(obj) {
    return Object.prototype.toString.call(obj) === '[object Object]';
}

function assertType(value, type) {
    let valid;
    const expectedType = getType(type);
    if (simpleCheckRE.test(expectedType)) {
        const t = typeof value;
        valid = t === expectedType.toLowerCase();
        // for primitive wrapper objects
        if (!valid && t === 'object') {
            valid = value instanceof type;
        }
    } else if (expectedType === 'Object') {
        valid = isPlainObject(value);
    } else if (expectedType === 'Array') {
        valid = Array.isArray(value);
    } else {
        try {
            valid = value instanceof type;
        } catch (e) {
            valid = false;
        }
    }
    return {
        valid,
        expectedType,
    };
}

function isNullOrUndefiend(val) {
    return val === null || val === undefined;
}

function parseValue(key, rule, data) {
    // 转换方法 rule
    if (typeof rule === 'function') rule = newRule(rule);

    // 设置默认值
    if ('default' in rule && isNullOrUndefiend(data)) {
        data =
            typeof rule.default === 'function' ? rule.default() : rule.default;
    }

    // 如果不存在值
    if (isNullOrUndefiend(data) && rule.required === true) {
        throw '缺少必要参数：' + key;
    }

    const assertedType = assertType(data, rule.type);

    // 未知类型（通过校验）
    if (assertedType.expectedType === '') {
        return data;
    }
    // 验证不通过（null 和 undefiend 值也通过校验）
    else if (!isNullOrUndefiend(data) && !assertedType.valid) {
        throw `字段${key}验证不通过，应该为${assertedType.expectedType}类型`;
    }
    // 验证通过
    else return data;
}

// 将 rule 转为配置
function newRule(config) {
    let rule;

    // 完整配置 { type: String, required: false, default: '1' }
    if (isPlainObject(config)) {
        rule = {
            type: config.type,
            default: config.default,
            required: config.required || false,
        };
    }
    // 方法类型（用于延迟读取默认值 如 token 和 userId）
    else if (typeof config === 'function') {
        if (getType(config)) {
            rule = { type: config, required: false };
        } else {
            const val = config();

            rule = {
                type: isNullOrUndefiend(val) ? undefined : val.constructor,
                default: val,
                required: false,
            };
        }
    }
    // 简单类型（constructor）
    else {
        rule = {
            type: config.constructor,
            default: config,
            required: false,
        };
    }

    return rule;
}

// 将配置项转为标准的rule
export function newRules(configs) {
    // 空 或者 方法类型
    if (!configs || typeof configs === 'function') return configs;

    const rules = {};

    // 键值（自身属性，按照顺序）
    const keys = Object.getOwnPropertyNames(configs);

    // 遍历键值
    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        const config = configs[key];

        let rule;

        // 方法类型
        if (typeof config === 'function') {
            rule = config;
        }
        // 其他类型
        else {
            rule = newRule(config);
        }

        rules[key] = rule;
    }

    return rules;
}

/**
 * 数据处理
 * @param {{[key: string]: Function | {type: Function, required: Boolean, default: any}}} paramsConfig
 * @param {Array} paramsData
 * @returns {Object}
 */
export function fixData(paramsConfig, paramsData) {
    // 如果是空值
    if (isNullOrUndefiend(paramsData)) paramsData = [];

    // 对象转为数组
    if (!Array.isArray(paramsData)) paramsData = [paramsData];

    // 如果没有入参配置，传递返回第一个对象
    if (!paramsConfig) return paramsData[0];

    // 方法类型
    if (typeof paramsConfig === 'function') return paramsConfig(...paramsData);

    // 对象类型
    if (!isPlainObject(paramsConfig)) throw '不支持的类型！';

    // 空值
    if (paramsData.length === 0) {
        paramsData = {};
    }
    // 传递的一个数据，且数据为对象
    else if (paramsData.length === 1 && isPlainObject(paramsData[0])) {
        paramsData = paramsData[0];
    }

    // 返回值
    const retData = {};
    // 键值
    const keys = Object.getOwnPropertyNames(paramsConfig);
    // 遍历键值
    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];

        // 获取传递过来的数据（先从 index 取，后从 key 中取）
        let val = Array.isArray(paramsData) ? paramsData[i] : paramsData[key];
        // 类型转换和验证
        val = parseValue(key, paramsConfig[key], val);

        // 赋值有效数据
        if (!isNullOrUndefiend(val)) {
            retData[key] = val;
        }
    }
    return retData;
}
