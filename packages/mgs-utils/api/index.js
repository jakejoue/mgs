/*
 * @Author: 黄威
 * @Date: 2021-09-09 15:57:57
 * @LastEditors: 黄威
 * @LastEditTime: 2021-12-21 10:22:48
 * @Description: 定义后端请求api
 */
import axios from 'axios';
import http from '../request';
import { pick } from '../misc';
import { newRules, fixData } from './data';
import CONIFG from '@mgs/config';

// API请求的方法
const API = {
    async requset0(config, pure = false) {
        // 设置 baseURL
        if (!config.baseURL) {
            config.baseURL = CONIFG.API_URLS[config.url.split('/')[0]];
        }

        return (pure ? axios : http)
            .request(config)
            .then(res => res.data)
            .then(res => {
                // 通用类型
                if ('code' in res) {
                    if (res.code === 200 || res.code === 'SUCCESS') {
                        return res.data;
                    } else {
                        throw res.msg;
                    }
                }
                // 尝试返回data类型
                return res.data === null || res.data === undefined
                    ? res
                    : res.data;
            });
    },
    /**
     * 请求的方法
     * @param {String} url 请求的服务地址
     * @param {*} data 数据
     * @param {*} method 请求使用的方法
     * @param {*} timeout 超时设置
     * @returns {Promise} 请求返回值
     */
    async requset(url, data, method = 'post', timeout) {
        const config = {
            url,
            method,
            [method !== 'get' ? 'data' : 'params']: data,
            timeout,
        };

        return this.requset0(config);
    },
    // 批处理
    async processBatch(ports, datas) {
        let id_lst = ports;
        let params_lst = datas;

        // 如果只传入一个参数 [ [port, data], [port, data], ... ]
        if (datas === undefined) {
            id_lst = [];
            params_lst = [];

            for (let i = 0; i < ports.length; i++) {
                const pd = ports[i];

                id_lst.push(pd[0]);
                params_lst.push(pd[1]);
            }
        }
        return this.requset('api/dataprocess/service/processBatch', {
            id_lst: id_lst,
            params_lst: params_lst,
        });
    },
};

function getAPI(apiConfig, pure) {
    const {
        // 端口加模式（适用 dataquery 配置的接口）
        port,
        mode = 'processOne',

        // 自定义url模式（适用自定义查询配置）
        url,

        // url 和 method
        method = 'post',

        // 数据相关（数据类型验证 和 params字段，用于body）
        data,
        params,

        // 请求生命周期函数
        fixArgs = (...rest) => rest,
        request = config => config, // 请求前
        response = config => config, // 响应前
    } = apiConfig;

    // url处理（先取自定义url）
    let url1 = url;

    // 如果是 dataquery 配置接口
    if (!url1) {
        if (mode === 'processOne') {
            url1 = `api/dataprocess/service/processOne/${port}`;
        } else {
            url1 = `api/dataquery/service/${mode}/${port}`;
        }
    }

    const rules = newRules(data);

    const api = async function (...rest) {
        const args = fixArgs(...rest);

        // 构造config
        let config = { url: url1, method, args };

        // 赋值判断
        const vals = fixData(rules, args);
        config[method !== 'get' ? 'data' : 'params'] = vals;

        // post请求方法类型取值params
        if (method === 'post' && params) {
            config.params = pick(vals, params);

            params.forEach(key => delete vals[key]);
            if (Object.keys(vals).length === 0) delete config.data;
        }

        // 附加默认请求器
        config.requestHandler = API.requset0;

        // 请求前处理
        config = request(config);

        // 发送请求 响应 返回值
        return config
            .requestHandler(config, pure)
            .then(res => response(res, config));
    };

    // 绑定批处理支持函数
    if (mode === 'processOne') {
        api.process = function (...rest) {
            const args = fixArgs(...rest);
            const vals = fixData(rules, args);
            return [port, vals];
        };
    }

    return api;
}

/**
 * 根据需求构造API的方法
 * @template T
 * @param {T} apiConfig
 * @param {Boolean} pure 是否为纯净请求模式
 * @returns {{ [key in keyof T]: (...rest: any[]) => Promise<any> }}
 */
export function buildApi(apiConfig, pure) {
    const customApi = {};

    for (const key in apiConfig) {
        customApi[key] = getAPI(apiConfig[key], pure);
    }

    return customApi;
}

export default API;
