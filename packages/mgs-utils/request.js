/*
 * @Author: 黄威
 * @Date: 2021-03-25 10:41:10
 * @LastEditors: 黄威
 * @LastEditTime: 2021-12-21 09:46:47
 * @Description: 请求用的工具类
 */
import axios from 'axios';
import CONFIG from '@mgs/config';
import { getQueryString } from './misc';

// post默认请求头
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8';

// 获取token，最开始从 url 中获取，后面从 localStorage 中获取
export function getToken() {
    const token =
        getQueryString('token') ||
        getQueryString('current_token') ||
        localStorage.getItem('token');

    return token;
}

// 验证头肯是否失效
export async function checkToken() {
    const token = getToken();

    const promise = token
        ? axios.get(CONFIG.CHECK_TOKEN_URL + '?token=' + token)
        : Promise.reject();

    return promise
        .then(res => {
            if (res.status !== 200 || !res.data) throw 'TOKEN验证不通过！';
        })
        .then(() => {
            localStorage.setItem('token', token);
        })
        .catch(() => {
            localStorage.removeItem('token');
            throw 'TOKEN验证不通过！';
        });
}

// 创建默认请求器
const service = axios.create({
    withCredentials: true,
    timeout: 50000,
});

// HTTPrequest拦截
service.interceptors.request.use(config => {
    // token
    config.headers['Authorization'] = 'bearer ' + getToken();

    // get 添加日期戳
    if (config.method == 'get') {
        config.params = {
            _t: new Date().getTime(),
            ...(config.params || {}),
        };
    }
    return config;
});

export default service;
