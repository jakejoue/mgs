/*
 * @Author: 黄威
 * @Date: 2021-11-24 09:46:33
 * @LastEditors: 黄威
 * @LastEditTime: 2021-11-24 16:46:49
 * @Description: hdfs相关方法
 */

import CONFIG from '@mgs/config';

/**
 * 获取完整的文件访问路径
 * @param {String} url hdfs文件路径
 * @returns String
 */
export function getPath(url) {
    let nurl = url;
    if (url.indexOf('?') !== -1) {
        nurl = url.slice(0, url.indexOf('?'));
    }

    return CONFIG.IMG_URL + nurl + CONFIG.IMG_AFTER_IMGURL;
}
