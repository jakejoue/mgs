/*
 * @Author: 黄威
 * @Date: 2021-12-10 10:04:10
 * @LastEditors: 黄威
 * @LastEditTime: 2022-03-04 14:01:50
 * @Description: Vue 属性装饰器
 */

import { titleCase, typeOf } from './misc';

/**
 * 双向属性绑定
 * @param {String} propName 属性名称
 * @param {*} option 参数
 * @param {Boolean} isModel 是否支持 v-model
 * @returns vue组件mixins
 */
function proxyProp(propName, option, isModel = false) {
    const key = 'current' + titleCase(propName);

    const propMixin = {
        props: { [propName]: option },
        data() {
            return {
                [key]: this[propName],
            };
        },
        watch: {
            // 外部变量
            [propName]: function (value) {
                this[key] = value;
            },
            // 内部变量
            [key]: function (value) {
                this.$emit('update:' + propName, value);
                // 如果是 v-model 变量
                if (isModel) this.$emit('input', value);
            },
        },
    };

    // 如果是 v-model 变量
    if (isModel) propMixin.model = { prop: propName };

    return propMixin;
}

/**
 * 提供 ref 引用
 * @param  {...string} keys
 * @returns
 */
export function ref(...keys) {
    const computed = {};

    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];

        computed[key] = {
            cache: false,
            get() {
                return this.$refs[key];
            },
        };
    }

    return { computed };
}

/**
 * 提供 双向绑定属性
 * @param {{[key:string]:any} | string[]} options
 */
export function propSync(options) {
    // 判断传入的数据是否未数组
    const ia = typeOf(options) === 'array';
    const mixins = [];

    for (const propName in options) {
        const option = ia ? {} : options[propName];
        mixins.push(proxyProp(propName, option));
    }

    return { mixins };
}

/**
 * 提法 vModel属性
 * @param {String} propName
 * @param {Object} option
 * @returns
 */
export function vModel(propName, option = {}) {
    return proxyProp(propName, option, true);
}

/**
 * slot渲染
 * @param {String} slot slot名称
 * @param {String} comp 组件名称
 * @param {Boolean} renderDefault 渲染默认插槽
 * @returns
 */
export function slotRender(slot = 'slot', comp = 'comp', renderDefault = true) {
    return {
        functional: true,
        inheritAttrs: false,
        props: ['root', 'item'],
        render(h, ctx) {
            const _slot = ctx.props.item[slot];
            const _comp = ctx.props.item[comp];

            // 如果是个slot
            if (_slot) {
                return ctx.props.root._t(_slot, null, ctx.data.attrs);
            }
            // 如果是个自定义组件
            else if (_comp) {
                return h(
                    _comp,
                    ctx._g(
                        ctx._b({ tag: _comp }, _comp, ctx.data.attrs, false),
                        ctx.listeners
                    )
                );
            }
            // 默认插槽
            else if (renderDefault) {
                return ctx.props.root._t('default', null, {
                    item: ctx.props.item,
                });
            }
        },
    };
}
