/*
 * @Author: 黄威
 * @Date: 2021-11-23 10:17:50
 * @LastEditors: 黄威
 * @LastEditTime: 2021-11-27 22:23:36
 * @Description: 核心库 模块化入口
 */

// 核心渲染库
import MgsApp from './mgs-app.vue';

// 注册核心库
export default {
    install(Vue) {
        Vue.component('MgsApp', MgsApp);
    },
};
