/*
 * @Author: 黄威
 * @Date: 2021-11-26 14:20:10
 * @LastEditors: 黄威
 * @LastEditTime: 2022-01-07 14:17:09
 * @Description: 核心驱动器
 */

import Vue from 'vue';
import { error, warn } from '@mgs/utils/log';
import { pick, extend } from '@mgs/utils/misc';

/**
 * 常量
 * @param {MgsComp} mgsComp
 * @param {Vue} target
 * @returns
 */
function newData(mgsComp, target) {
    // 常量解析
    const parseConstant = function (config) {
        const { type, value } = config;
        // 表达式类型（不可被序列化的类型）
        if (type === 'expression') {
            return Function('return ' + value).call(target);
        }
        // 常规类型（可以被序列化的类型，JSON.stringify）
        else {
            return value;
        }
    };

    const constants = {};

    const { props = [] } = mgsComp.data;
    for (let i = 0; i < props.length; i++) {
        const { key, type, config } = props[i];

        // 常量
        if (type === 'Constant') {
            constants[key] = parseConstant(config);
        }
    }

    return constants;
}
/**
 * 计算变量
 * @param {MgsComp} mgsComp
 * @returns
 */
function newComputed(mgsComp) {
    // 解析计算属性
    const parseComputed = function (config) {
        return function () {
            const { linkid, expression } = config;
            const linkMgsComp = mgsComp.engin.getComp(linkid);

            if (!linkMgsComp || !linkMgsComp.vm) return;

            const exp = expression ? '.' + expression : '';
            return Function('return this' + exp).call(linkMgsComp.vm);
        };
    };

    const computed = {};

    const { props = [] } = mgsComp.data;
    for (let i = 0; i < props.length; i++) {
        const { key, type, config } = props[i];

        // 常量
        if (type === 'Computed') {
            computed[key] = parseComputed(config);
        }
    }

    return computed;
}
/**
 * 事件
 * @param {MgsComp} sender
 * @returns
 */
function newEvents(sender) {
    // 事件解析
    const parseEvent = function (event) {
        const { key, callback, receivers } = event;

        return async function (...rest) {
            // 事件包装
            const evt = {
                key,
                sender,
                args: rest,
            };

            // 自定义回调
            if (callback) {
                try {
                    let func = callback;
                    // string 转 function
                    if (typeof callback === 'string') {
                        func = Function('evt', callback);
                    }
                    // 执行函数
                    func(evt);
                } catch (err) {
                    error('[func]执行失败：', err, event);
                }
            }
            // 接收器模式
            else if (receivers) {
                for (let i = 0; i < receivers.length; i++) {
                    const { id, key, sync = false, data } = receivers[i];

                    const args = data || rest;

                    // 同步模式
                    if (sync) {
                        await sender.engin.postMessage(id, key, ...args);
                    }
                    // 异步模式
                    else {
                        sender.engin.postMessage(id, key, ...args);
                    }
                }
            }
        };
    };

    const eventsMap = {};

    const { events = [] } = sender.data;
    for (let i = 0; i < events.length; i++) {
        const { key } = events[i];
        eventsMap[key] = parseEvent(events[i]);
    }

    return eventsMap;
}

/**
 * 注册一个组件
 * @param {MgsComp} mgsComp
 */
function registerComponent(mgsComp) {
    const { name, props = [] } = mgsComp.data;
    const propKeys = props.map(p => p.key);
    const events = newEvents(mgsComp);

    // 注册一个 id 组件
    Vue.component(mgsComp.id, {
        // 标识是否为代理组件
        proxy: true,
        // 隐藏内联属性
        inheritAttrs: false,
        data() {
            return newData(mgsComp, this);
        },
        computed: newComputed(mgsComp),
        mounted() {
            // 派发初始化完成事件
            mgsComp.vm = this.$children[0];
            mgsComp.vm.$emit('mounted');
        },
        beforeDestroy() {
            // 派发组件销毁事件
            mgsComp.vm.$emit('destroyed');
            mgsComp.vm = null;
        },
        methods: {
            getVm() {
                let vm = this.$children[0];

                while (vm && vm.$options.proxy) {
                    vm = vm.$children[0];
                }

                return vm;
            },
        },
        render(h) {
            return h(
                name,
                this._g(
                    this._b(
                        { tag: 'component' },
                        'component',
                        extend({}, this.$attrs, pick(this, propKeys)),
                        false
                    ),
                    extend({}, this._events, events)
                )
            );
        },
    });
}

class MgsComp {
    constructor(options) {
        this.level = 0;

        /** @type {ComponentConfig} */ this.data = null;
        /** @type {MgsComp} */ this.parent = null;

        // 设置
        Object.assign(this, options);

        // internal
        /** @type {Vue} */ this.vm = null;
        this.level = 0;
        this.childComps = [];

        if (this.parent) {
            this.level = this.parent.level + 1;
        }

        // engin
        /** @type {LayoutEngin} */ const engin = this.engin;
        if (!engin) {
            throw new Error('[MgsComp]engin is required!');
        }

        // 注册组件
        if (this.level !== 0) {
            registerComponent(this);
            engin.registerComp(this);
        }

        // 设置子数据
        this.setData(options.data);
    }

    get id() {
        return !this.data || !this.data.id ? 'root' : this.data.id;
    }

    setData(data) {
        this.data = data;
        this.childComps = [];

        let children;

        if (this.level === 0 && this.data instanceof Array) {
            children = this.data;
        } else {
            children = data.children || [];
        }

        for (let i = 0, j = children.length; i < j; i++) {
            this.childComps.push(
                new MgsComp({
                    parent: this,
                    engin: this.engin,
                    data: children[i],
                })
            );
        }
    }
}

export default class LayoutEngin {
    /**
     * 布局引擎
     * @param {SchemeConfig} options
     */
    constructor(options) {
        const { id, entry } = options;

        this.id = id;
        this.entryId = entry;

        /**
         * 组件键值对
         * @type {{[key:string]:MgsComp}}
         */
        this.componentsMap = {};

        // 设置root
        this.root = new MgsComp({
            engin: this,
            data: options.components,
        });
    }

    get entry() {
        return this.componentsMap[this.entryId] ? this.entryId : '';
    }

    // 注册组件
    registerComp(comp) {
        this.componentsMap[comp.id] = comp;
    }

    // 取消注册组件
    deregisterComp(comp) {
        comp.childComps.forEach(child => {
            this.deregisterNode(child);
        });

        delete this.nodesMap[comp.id];
    }

    // 获取组件
    getComp(key) {
        return this.componentsMap[key];
    }

    /**
     * 派发消息
     * @param {String} key 对象id
     * @param {String} handlerName 接口名称
     * @param  {...any} rest 数据
     * @returns
     */
    postMessage(key, handlerName, ...rest) {
        const target = this.getComp(key);

        if (!target || !target.vm) {
            return warn('[MgsComp]vm对象不存在或未初始化', key, target);
        }

        if (typeof target.vm[handlerName] !== 'function') {
            return warn('[MgsComp]vm不存在指定的接口', handlerName, target);
        }

        try {
            return target.vm[handlerName](...rest);
        } catch (err) {
            error('[MgsComp]vm执行失败', err, ...arguments);
        }
    }
}
