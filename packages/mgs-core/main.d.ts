/*
 * @Author: 黄威
 * @Date: 2021-11-11 17:44:01
 * @LastEditors: 黄威
 * @LastEditTime: 2022-02-07 10:25:16
 * @Description: 数据类型定义
 */

// 数据类型（订阅器发布器判断）
declare type DataType = 'string' | 'number' | 'boolean' | 'vue';

declare type ConstantConfig = {
    // 常量类型（不同类型采用不同的反序列化方法）
    type:
        | 'boolean'
        | 'number'
        | 'string'
        | 'object'
        | 'function'
        | 'expression'; //表达式类型（会尝试 eval）
    // 数组
    value: any;
};

declare type ComputedConfig = {
    // 引用 vm 的 id
    linkid: string;
    // 取用值（为空会直接返回 引用vm示例）
    expression?: string;
};

// 组件配置
declare type ComponentConfig = {
    // 数据id
    id: string;

    // 组件名称
    name: string;

    // 组件别名
    alias?: string;

    // 组件类别 基础 布局 工具
    type: 'component' | 'layout' | 'tool';

    // 组件路径
    path: string;

    // 描述
    description?: string;

    // 启用状态
    state: boolean;

    // 可配置项
    props: {
        // 属性键
        key: string;
        // 属性名称描述
        name: string;
        // 类型（常量 -> 或 <- 数据驱动-计算属性）
        type: 'Constant' | 'Computed';
        // 配置项（可选项等）
        config: ConstantConfig | ComputedConfig;
    }[];

    // 订阅器（支持的接口）
    events: {
        // 事件键
        key: string;
        // 事件名称
        name: string;
        // 入参
        params: DataType[];

        // 接收者
        receivers?: {
            // 接收者id
            id: string;
            // 事件键
            key: string;
            // 事件名称
            name: string;
            // 是否为同步事件，即等待当事件执行完毕，才执行下个接收器
            sync?: boolean;
            // 自定义参数（会覆盖原事件的入参）
            data?: any[];
        }[];

        // 回调
        callback?: string | ((evt: any) => void);
    }[];

    // 容器类型（如toolbar）
    children: ComponentConfig[];
};

// 方案配置
declare type SchemeConfig = {
    id: string;

    // 名称
    name: string;

    // 描述
    description?: string;

    // 启用状态
    state: boolean;

    // 创建时间
    createtime: string;

    // 入口组件id 一般为布局组件
    entry: string;

    // 内置的模板布局
    components: ComponentConfig[];
};
