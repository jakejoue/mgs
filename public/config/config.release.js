/*
 * @Author: 黄威
 * @Date: 2021-09-16 14:35:27
 * @LastEditors: wanghan
 * @LastEditTime: 2022-01-18 17:54:27
 * @Description: 发布测试时候的配置项（用于内外测试）
 */
var APPCONFIG = APPCONFIG || {};
(function () {
    APPCONFIG = {
        // 接口请求相关
        SERVER_IP: GLOBECONFIG.Sites.ApiProxy,
        // dfc服务相关配置
        DFC_IP: GLOBECONFIG.Sites.DFC_URL || `http://192.168.3.181:8083/geoesb/proxy/3f8c35c00c2241798ebbd8d5378a1e64/886e60bb7e014f22a707de23e6f6505d`,

        // 验证相关
        CHECK_TOKEN_URL: `${GLOBECONFIG.Sites.GetTokenUrl}/api/auth/oauth/check_token`, // token校验
        LOGIN_URL: GLOBECONFIG.Sites.SiteHost.WebHost, // 登录界面

        // hdfs相关
        IMG_URL: GLOBECONFIG.Sites.IMG_URL,
        IMG_AFTER_IMGURL: GLOBECONFIG.Sites.AFTER_IMGURL,

        // 地图代理相关
        PROXY_URL: GLOBECONFIG.Sites.ProxyUrl,
        PLOT_URL: GLOBECONFIG.Sites.PLOT_URL,

        // 分析服务配置
        ANALYSIS_CONFIG: GLOBECONFIG.Sites.ANALYSIS_CONFIG || {
            url: 'http://192.168.3.181:8000/api/analysis',
            dataset: 'DEM25',
            datasource: 'test',
            fileName: 'test2',
        },
    };
})();
