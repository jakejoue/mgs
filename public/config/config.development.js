/*
 * @Author: 黄威
 * @Date: 2021-09-16 14:32:52
 * @LastEditors: wanghan
 * @LastEditTime: 2022-01-21 15:55:30
 * @Description: 开发时候用的配置项（用于本地开发）
 */
var APPCONFIG = APPCONFIG || {};
(function () {
    var BASE_IP = 'http://192.168.3.181'; // 北京
    var Proxy_Port = '8083';
    // var BASE_IP = 'http://172.16.12.205' // 武汉
    // var Proxy_Port = '8001'

    APPCONFIG = {
        // 接口请求相关
        SERVER_IP: `${BASE_IP}:8000`,
        // dfc查询相关
        // DFC_IP: `http://192.168.3.182:8761`,
        DFC_IP: `http://192.168.3.181:8083/geoesb/proxy/3f8c35c00c2241798ebbd8d5378a1e64/886e60bb7e014f22a707de23e6f6505d`,

        // 验证相关
        CHECK_TOKEN_URL: `${BASE_IP}:8000/api/auth/oauth/check_token`, // token校验
        LOGIN_URL: `${BASE_IP}:8088/mgs/#/login`, // 登录界面

        // hdfs相关
        IMG_URL: `http://192.168.3.183:9864`, // 算法模型中静态图片地址
        IMG_AFTER_IMGURL: `?op=OPEN&namenoderpcaddress=ceph27:9000&offset=0`, // hadoop相关文件下载地址 接口后缀

        // 地图代理相关
        PROXY_URL: `${BASE_IP}:${Proxy_Port}`,
        PLOT_URL:  `${BASE_IP}:8090/iserver/services/plot-jingyong/rest/plot`,

        // 分析服务配置
        // ANALYSIS_CONFIG: {
        //     url: 'http://192.168.3.181:8000/api/analysis',
        //     dataset: 'DEM25',
        //     datasource: 'test',
        //     fileName: 'test2',
        // },
        ANALYSIS_CONFIG: {
            url: 'http://192.168.3.181:8000/api/analysis',
            dataset: 'JingjinTerrain',
            datasource: 'BeijingNetwork',
            fileName: 'DashboardNavigation',
        },
    };
})();
