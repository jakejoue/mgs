/*
 * @Author: 黄威
 * @Date: 2021-11-15 14:35:50
 * @LastEditors: wanghan
 * @LastEditTime: 2022-01-18 17:50:29
 * @Description: vue项目配置文件
 */
// const path = require('path');
const isDebug = process.env.NODE_ENV !== 'production';

module.exports = {
    publicPath: './',
    filenameHashing: isDebug,
    productionSourceMap: isDebug,
    chainWebpack: config => {
        // 设置别名
        // config.resolve.alias.set('@mgs', path.join(__dirname, '/@mgs'));
        // 修改html插件的配置（用于动态获取配置文件）
        config.plugin('html').tap(args => {
            args[0].meta = { MODE: process.env.MODE.trim() };
            return args;
        });
    },
    configureWebpack: {
        externals: [
            /^cesium$/i,
            {
                leaflet: 'L',
                '@supermap/iclient-leaflet': 'L.supermap',
            },
        ],
    },
    devServer: {
        port: 7777,
        proxy: {
            // dfc
            '/dfc': {
                target: 'http://192.168.3.182:8761',
            },
        },
    },
};
